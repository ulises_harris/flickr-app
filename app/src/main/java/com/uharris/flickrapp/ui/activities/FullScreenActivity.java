package com.uharris.flickrapp.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.routing.FlickrAppRouting;
import com.uharris.flickrapp.data.sections.fullscreen.FullScreenContract;
import com.uharris.flickrapp.data.sections.fullscreen.FullScreenPresenter;
import com.uharris.flickrapp.util.ImageUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FullScreenActivity extends AppCompatActivity implements FullScreenContract.View {

    @BindView(R.id.photoImageView)
    ImageView photoImageView;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.photoTitle)
    TextView photoTitle;
    @BindView(R.id.commentsTotal)
    TextView commentsTotal;
    @BindView(R.id.activity_full_screen)
    RelativeLayout activityFullScreen;
    private Photo photo;

    FullScreenContract.ActionListener presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photo = bundle.getParcelable(FlickrAppRouting.PHOTO_OBJECT);
        }

        presenter = new FullScreenPresenter(this, new FlickrAppRouting());
        presenter.getComments(photo.getId());
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @OnClick(R.id.close)
    public void onClick() {
        onBackPressed();
    }

    @Override
    public void onGetCommentsSuccess(Response<Comments> response) {
        Picasso.with(this).load(ImageUtil.getPhotoUrl(this, photo, "c")).into(photoImageView);
        userName.setText(photo.getOwnerName());
        photoTitle.setText(photo.getTitle());
        if (response.getStat().equals("ok")) {
            if(response.getItem().getComments() != null)
                commentsTotal.setText(getString(R.string.comments_total, response.getItem()
                    .getComments().size()));
            else
                commentsTotal.setText(getString(R.string.comments_total, 0));
        }
    }

    @Override
    public void onGetCommentsFailure(String error) {
        Snackbar.make(activityFullScreen, error, Snackbar.LENGTH_SHORT).show();
    }
}
