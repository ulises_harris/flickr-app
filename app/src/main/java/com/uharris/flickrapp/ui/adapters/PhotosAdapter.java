package com.uharris.flickrapp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.ui.holders.PhotoGridViewHolder;
import com.uharris.flickrapp.ui.holders.PhotoListViewHolder;

import java.util.ArrayList;

/**
 * Created by uharris on 2/13/17.
 */

public class PhotosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    ArrayList<Photo> photos = new ArrayList<>();
    int isGrid;
    PhotoAdapterListener listener;

    public PhotosAdapter(Context context, int isGrid) {
        this.context = context;
        this.isGrid = isGrid;
    }

    @Override
    public int getItemViewType(int position) {
        return (isGrid == 1 ? 1 : 0);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 1){
            View v = LayoutInflater.from(context).inflate(R.layout.photo_grid_item,
                    parent, false);
            return new PhotoGridViewHolder(v);
        }else{
            View v = LayoutInflater.from(context).inflate(R.layout.photo_list_item,
                    parent, false);
            return new PhotoListViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Photo photo = photos.get(position);
        if(isGrid == 1){
            final PhotoGridViewHolder gridHolder = (PhotoGridViewHolder) holder;
            gridHolder.configureItem(photo);
            if(listener != null){
                gridHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClickPhoto(photo, v, gridHolder.getAdapterPosition());
                    }
                });
            }
        }else{
            final PhotoListViewHolder listHolder = (PhotoListViewHolder) holder;
            listHolder.configureItem(photo);
            if(listener != null){
                listHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClickPhoto(photo, v, listHolder.getAdapterPosition());
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(ArrayList<Photo> photoArrayList){
        if(photoArrayList != null){
            photos = photoArrayList;
            notifyDataSetChanged();
        }
    }

    public void addPhotos(ArrayList<Photo> photoArrayList){
        if(photoArrayList != null){
            photos.addAll(photoArrayList);
            notifyDataSetChanged();
        }
    }

    public void setManager(int isGrid){
        this.isGrid = isGrid;
        notifyDataSetChanged();
    }

    public interface PhotoAdapterListener{
        void onClickPhoto(Photo photo, View v, int position);
    }

    public void setListener(PhotoAdapterListener listener) {
        this.listener = listener;
    }
}
