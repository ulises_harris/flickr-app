package com.uharris.flickrapp.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.routing.FlickrAppRouting;
import com.uharris.flickrapp.data.sections.main.MainContract;
import com.uharris.flickrapp.data.sections.main.MainPresenter;
import com.uharris.flickrapp.ui.adapters.PhotosAdapter;
import com.uharris.flickrapp.util.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View,
        SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener,
        PhotosAdapter.PhotoAdapterListener{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.photosRecyclerView)
    RecyclerView photosRecyclerView;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.photosRefreshLayout)
    SwipeRefreshLayout photosRefreshLayout;

    MainContract.ActionListener presenter;
    int page = 1;
    PhotosAdapter adapter;
    int isGrid = 1;

    private EndlessRecyclerViewScrollListener gridScrollListener;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager listLayoutManager;
    private EndlessRecyclerViewScrollListener listScrollListener;
    private MenuItem searchMenuItem;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        setUpRecyclerView();

        showDialog(true);
        presenter = new MainPresenter(this, new FlickrAppRouting());
        presenter.getPublicPhotos(page);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_layout){
            if(isGrid == 1){
                isGrid = 0;
                photosRecyclerView.setLayoutManager(listLayoutManager);
                photosRecyclerView.removeOnScrollListener(gridScrollListener);
                photosRecyclerView.addOnScrollListener(listScrollListener);
                item.setIcon(R.drawable.ic_list_menu);
            }else{
                isGrid = 1;
                photosRecyclerView.setLayoutManager(gridLayoutManager);
                photosRecyclerView.removeOnScrollListener(listScrollListener);
                photosRecyclerView.addOnScrollListener(gridScrollListener);
                item.setIcon(R.drawable.ic_grid_menu);
            }

            adapter.setManager(isGrid);

        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpRecyclerView() {
        gridLayoutManager = new GridLayoutManager(this, 3);
        listLayoutManager = new LinearLayoutManager(this);
        photosRecyclerView.setLayoutManager(gridLayoutManager);
        adapter = new PhotosAdapter(this, isGrid);
        adapter.setListener(this);
        photosRecyclerView.setAdapter(adapter);
        photosRefreshLayout.setOnRefreshListener(this);
        gridScrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                page += 1;
                if(mSearchView.isIconified())
                    presenter.getPublicPhotos(page);
                else
                    presenter.searchPhotos(page, mSearchView.getQuery().toString());
            }
        };

        listScrollListener = new EndlessRecyclerViewScrollListener(listLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                page += 1;
                if(mSearchView.isIconified())
                    presenter.getPublicPhotos(page);
                else
                    presenter.searchPhotos(page, mSearchView.getQuery().toString());
            }
        };

        photosRecyclerView.addOnScrollListener(gridScrollListener);
    }


    @Override
    public void onSuccess(Response<Photos> response) {
        if(response.getStat().equals("ok")){
            showDialog(false);
            if(photosRefreshLayout.isRefreshing())
                photosRefreshLayout.setRefreshing(false);
            if(response.getItem().getPage() > 1){
                adapter.addPhotos(response.getItem().getPhotoList());
            }else{
                adapter.setPhotos(response.getItem().getPhotoList());
            }
        }else{
            Snackbar.make(coordinatorLayout, "There was an error connecting to the server. Try " +
                    "again later", Snackbar.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFailure(String error) {
        showDialog(false);
        Snackbar.make(coordinatorLayout, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showDialog(boolean show) {
        if(show){
            progressBar.setVisibility(View.VISIBLE);
            photosRecyclerView.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.GONE);
            photosRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        page = 1;
        adapter.setPhotos(new ArrayList<Photo>());
        if(isGrid == 1)
            gridScrollListener.resetState();
        else
            listScrollListener.resetState();
        if(mSearchView.isIconified()){
            presenter.getPublicPhotos(page);
        }else{
            presenter.searchPhotos(page, mSearchView.getQuery().toString());
        }

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query.equals("")) {
            return false;
        } else {
            this.page = 1;
            showDialog(true);
            mSearchView.clearFocus();
            presenter.searchPhotos(page, query);
            if(isGrid == 1)
                gridScrollListener.resetState();
            else
                listScrollListener.resetState();
            return true;
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onClickPhoto(Photo photo, View v, int position) {
        presenter.onPhotoDetail(this, photo);
    }
}
