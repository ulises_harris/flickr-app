package com.uharris.flickrapp.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.routing.FlickrAppRouting;
import com.uharris.flickrapp.data.sections.detail.DetailContract;
import com.uharris.flickrapp.data.sections.detail.DetailPresenter;
import com.uharris.flickrapp.util.DateUtil;
import com.uharris.flickrapp.util.ImageUtil;
import com.uharris.flickrapp.util.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailActivity extends AppCompatActivity implements DetailContract.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_detail)
    RelativeLayout contentDetail;
    @BindView(R.id.photoImageView)
    ImageView photoImageView;
    @BindView(R.id.userIcon)
    CircleImageView userIcon;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.photoTitle)
    TextView photoTitle;
    @BindView(R.id.photoInfo)
    LinearLayout photoInfo;
    @BindView(R.id.photoDate)
    TextView photoDate;
    @BindView(R.id.userInfo)
    RelativeLayout userInfo;
    @BindView(R.id.photoDescription)
    TextView photoDescription;
    @BindView(R.id.imageLocation)
    ImageView imageLocation;
    @BindView(R.id.scrollDetail)
    NestedScrollView scrollDetail;
    @BindView(R.id.photoLocation)
    TextView photoLocation;
    private Photo photo;
    private DetailContract.ActionListener presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            photo = bundle.getParcelable(FlickrAppRouting.PHOTO_OBJECT);
        }

        presenter = new DetailPresenter(this, new FlickrAppRouting());
        presenter.getPlaceDetail(photo.getPlaceId());
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGetPlaceDetailSuccess(Response<Place> response) {
        Picasso.with(this).load(ImageUtil.getPhotoUrl(this, photo, "z")).into(photoImageView);
        Picasso.with(this).load(ImageUtil.getBuddyIconUrl(this, photo)).into(userIcon);
        userName.setText(photo.getOwnerName());
        photoTitle.setText(photo.getTitle());
        photoDescription.setText(StringUtils.getHtmlString(photo.getDescription().getContent()));
        photoDescription.setMovementMethod(LinkMovementMethod.getInstance());
        photoDate.setText(DateUtil.getDate(photo.getDateUpload()));
        if (response.getStat().equals("ok")) photoLocation.setText(response.getItem().getName());
    }

    @Override
    public void onGetPlaceDetailFailure(String error) {
        Snackbar.make(scrollDetail, error, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.photoImageView)
    public void onClick() {
        presenter.fullScreenActivity(this, photo);
    }
}
