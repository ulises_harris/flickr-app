package com.uharris.flickrapp.ui.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.util.DateUtil;
import com.uharris.flickrapp.util.ImageUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by uharris on 2/13/17.
 */

public class PhotoListViewHolder extends RecyclerView.ViewHolder {

    private Context context;

    @BindView(R.id.photoImageView)
    ImageView photoImageView;
    @BindView(R.id.userIcon)
    CircleImageView userIcon;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.photoTitle)
    TextView photoTitle;
    @BindView(R.id.photoDate)
    TextView photoDate;

    public PhotoListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
    }

    public void configureItem(Photo item) {
        Picasso.with(context)
                .load(ImageUtil.getPhotoUrl(context, item, "m"))
                .into(photoImageView);
        String buddyIconUrl = ImageUtil.getBuddyIconUrl(context, item);
        Picasso.with(context)
                .load(buddyIconUrl)
                .into(userIcon);
        photoTitle.setText(item.getTitle());
        userName.setText(item.getOwnerName());
        photoDate.setText(DateUtil.getDate(item.getDateUpload()));
    }
}
