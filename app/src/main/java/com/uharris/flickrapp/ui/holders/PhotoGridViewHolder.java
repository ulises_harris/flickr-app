package com.uharris.flickrapp.ui.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.util.ImageUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by uharris on 2/13/17.
 */

public class PhotoGridViewHolder extends RecyclerView.ViewHolder {

    private Context context;

    @BindView(R.id.photoImageView)
    ImageView photoImageView;

    public PhotoGridViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = itemView.getContext();
    }

    public void configureItem(Photo item) {
        Picasso.with(context)
                .load(ImageUtil.getPhotoUrl(context, item, "m"))
                .into(photoImageView);
    }
}
