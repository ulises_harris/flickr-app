package com.uharris.flickrapp.util;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * Created by uharris on 2/14/17.
 */

public class StringUtils {

    public static Spanned getHtmlString(String html) {
        Spanned text;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            text = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            text = Html.fromHtml(html);
        }

        return text;
    }
}
