package com.uharris.flickrapp.util;

import android.content.Context;

import com.uharris.flickrapp.R;
import com.uharris.flickrapp.data.entities.Photo;

/**
 * Created by uharris on 2/13/17.
 */

public class ImageUtil {

    public static String getPhotoUrl(Context context, Photo photo, String size) {
        String photoUrl = context.getString(R.string.photo_url, photo.getFarm(), photo.getServer(),
                photo.getId(), photo.getSecret(), size);
        return photoUrl;
    }

    public static String getBuddyIconUrl(Context context, Photo photo) {
        String photoUrl = context.getString(R.string.buddy_icon_url, photo.getIconFarm(),
                photo.getIconServer(), photo.getOwner());
        return photoUrl;
    }
}

