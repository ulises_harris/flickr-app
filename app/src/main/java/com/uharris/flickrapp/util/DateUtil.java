package com.uharris.flickrapp.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by uharris on 2/8/17.
 */

public class DateUtil {

    public static String DATE_BD_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static String DATE_APP_FORMAT = "dd MMM";

    public static String getDate(String date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.valueOf(date));

        SimpleDateFormat newFormat = new SimpleDateFormat(DATE_APP_FORMAT);

        return newFormat.format(calendar.getTime());

    }
}
