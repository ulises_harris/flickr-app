package com.uharris.flickrapp.data.repositories;

import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.repositories.comments.CommentService;
import com.uharris.flickrapp.data.repositories.photos.PhotoService;
import com.uharris.flickrapp.data.repositories.places.PlacesService;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by uharris on 2/13/17.
 */

public class ServiceManager {

    private static final String BASE_URL = "https://api.flickr.com/services/rest/";

    private static final String API_KEY = "319070395ce106b551aa2da289139a03";

    private static Retrofit retrofit;

    private static ServiceManager instance = null;

    PhotoService photoService;

    PlacesService placesService;

    CommentService commentService;

    public ServiceManager() {
        this(BASE_URL);
    }

    public ServiceManager(String baseUrl){
        if (retrofit == null) {
            HttpLoggingInterceptor interceptorLog = new HttpLoggingInterceptor();
            interceptorLog.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    HttpUrl originalHttpUrl = original.url();

                    HttpUrl url = originalHttpUrl.newBuilder()
                            .addQueryParameter("api_key", API_KEY)
                            .addQueryParameter("format", "json")
                            .addQueryParameter("nojsoncallback", "1")
                            .build();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .url(url);

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            clientBuilder.addInterceptor(interceptorLog);
            OkHttpClient client = clientBuilder.build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

            photoService = retrofit.create(PhotoService.class);
            placesService = retrofit.create(PlacesService.class);
            commentService = retrofit.create(CommentService.class);
        }
    }

    public static ServiceManager getInstance() {
        if (instance == null) {
            instance = new ServiceManager();
        }

        return instance;
    }

    public PhotoService getPhotoService() {
        return photoService;
    }

    public PlacesService getPlacesService() {
        return placesService;
    }

    public CommentService getCommentService() {
        return commentService;
    }
}
