package com.uharris.flickrapp.data.sections.detail;

import android.app.Activity;

import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.interactors.detail.DetailInteractor;
import com.uharris.flickrapp.data.interactors.detail.DetailProvider;
import com.uharris.flickrapp.data.routing.IBaseRouting;

/**
 * Created by uharris on 2/14/17.
 */

public class DetailPresenter implements DetailContract.ActionListener, DetailProvider.DataOutput {

    DetailContract.View view;
    IBaseRouting router;

    DetailProvider.Interactor interactor;

    public DetailPresenter(DetailContract.View view, IBaseRouting router) {
        this.view = view;
        this.router = router;

        interactor = new DetailInteractor(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void getPlaceDetail(String id) {
        interactor.getPlaceDetail(id);
    }

    @Override
    public void fullScreenActivity(Activity activity, Photo photo) {
        router.fullScreen(activity, photo);
    }

    @Override
    public void onGetPlaceDetailSuccess(Response<Place> response) {
        if(view != null){
            view.onGetPlaceDetailSuccess(response);
        }
    }


    @Override
    public void onGetPlaceDetailFailure(String error) {
        if(view != null){
            view.onGetPlaceDetailFailure(error);
        }
    }
}
