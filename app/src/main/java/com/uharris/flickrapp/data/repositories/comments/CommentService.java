package com.uharris.flickrapp.data.repositories.comments;

import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by uharris on 2/14/17.
 */

public interface CommentService {

    @GET("?method=flickr.photos.comments.getList")
    Call<Response<Comments>> getComments(@Query("photo_id") String id);
}
