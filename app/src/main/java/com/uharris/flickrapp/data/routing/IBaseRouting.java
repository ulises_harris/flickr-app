package com.uharris.flickrapp.data.routing;

import android.app.Activity;

import com.uharris.flickrapp.data.entities.Photo;


/**
 * Created by Ulises.harris on 8/22/16.
 */
public interface IBaseRouting {

    void photoDetail(Activity activity, Photo photo);

    void fullScreen(Activity activity, Photo photo);
}
