package com.uharris.flickrapp.data.sections.fullscreen;

import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.interactors.comment.CommentInteractor;
import com.uharris.flickrapp.data.interactors.comment.CommentProvider;
import com.uharris.flickrapp.data.routing.IBaseRouting;

/**
 * Created by uharris on 2/14/17.
 */

public class FullScreenPresenter implements FullScreenContract.ActionListener, CommentProvider
        .DataOutput {

    FullScreenContract.View view;
    IBaseRouting router;
    CommentProvider.Interactor interactor;

    public FullScreenPresenter(FullScreenContract.View view, IBaseRouting router) {
        this.view = view;
        this.router = router;

        interactor = new CommentInteractor(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void getComments(String id) {
        interactor.getComments(id);
    }

    @Override
    public void onGetCommentsSuccess(Response<Comments> response) {
        if(view != null){
            view.onGetCommentsSuccess(response);
        }
    }

    @Override
    public void onGetCommentsFailure(String error) {
        if(view != null){
            view.onGetCommentsFailure(error);
        }
    }

}
