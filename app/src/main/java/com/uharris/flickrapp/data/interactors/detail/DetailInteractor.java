package com.uharris.flickrapp.data.interactors.detail;

import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.repositories.RepositoryListener;
import com.uharris.flickrapp.data.repositories.photos.PhotoRepositoryImpl;
import com.uharris.flickrapp.data.repositories.places.PlaceRepositoryImpl;

/**
 * Created by uharris on 2/14/17.
 */

public class DetailInteractor implements DetailProvider.Interactor {

    DetailProvider.DataOutput output;

    PlaceRepositoryImpl repository = new PlaceRepositoryImpl();

    public DetailInteractor(DetailProvider.DataOutput output) {
        this.output = output;
    }

    @Override
    public void getPlaceDetail(String id) {
        repository.getPlaceDetail(id, new RepositoryListener<Response<Place>>() {
            @Override
            public void onSuccess(Response<Place> response) {
                output.onGetPlaceDetailSuccess(response);
            }

            @Override
            public void onFailed(String error) {
                output.onGetPlaceDetailFailure(error);
            }
        });
    }
}
