package com.uharris.flickrapp.data.interactors.comment;

import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Response;

/**
 * Created by uharris on 2/14/17.
 */

public interface CommentProvider {

    interface Interactor{
        void getComments(String id);
    }

    interface DataOutput{
        void onGetCommentsSuccess(Response<Comments> response);

        void onGetCommentsFailure(String error);
    }
}
