package com.uharris.flickrapp.data.repositories.photos;

import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by uharris on 2/13/17.
 */

public interface PhotoService {

    @GET("?extras=date_upload,icon_server,owner_name,geo,description&method=flickr.photos" +
            ".getRecent")
    Call<Response<Photos>> getPublicPhotos(@Query("page") int page);

    @GET("?extras=date_upload,icon_server,owner_name,geo,description&method=flickr.photos" +
            ".search&privacy_filter=1")
    Call<Response<Photos>> searchPhotos(@Query("page") int page, @Query("text") String text);
}
