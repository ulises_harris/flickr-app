package com.uharris.flickrapp.data.routing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.ui.activities.DetailActivity;
import com.uharris.flickrapp.ui.activities.FullScreenActivity;

/**
 * Created by uharris on 2/13/17.
 */

public class FlickrAppRouting implements IBaseRouting {

    public static String PHOTO_OBJECT = "photo_object";

    @Override
    public void photoDetail(Activity activity, Photo photo) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PHOTO_OBJECT, photo);
        startActivity(activity, DetailActivity.class, bundle);
    }

    @Override
    public void fullScreen(Activity activity, Photo photo) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PHOTO_OBJECT, photo);
        startActivity(activity, FullScreenActivity.class, bundle);
    }

    public void startActivity(Activity activity, Class clazz) {
        Intent intent = new Intent(activity, clazz);
        activity.startActivity(intent);
    }

    public void startActivityTopClear(Context context, Class clazz) {
        Intent intent = new Intent(context, clazz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public void startActivityFinish(Activity activity, Class clazz) {
        Intent intent = new Intent(activity, clazz);
        activity.startActivity(intent);
        activity.finish();
    }

    public void startActivityFinish(Activity activity, Class clazz, Bundle bundle) {
        Intent intent = new Intent(activity, clazz);
        intent.putExtras(bundle);
        activity.startActivity(intent);
        activity.finish();
    }

    public void startActivity(Activity activity, Class clazz, Bundle bundle) {
        Intent intent = new Intent(activity, clazz);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }
}
