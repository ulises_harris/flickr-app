package com.uharris.flickrapp.data.interactors.comment;

import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.repositories.RepositoryListener;
import com.uharris.flickrapp.data.repositories.comments.CommentRepositoryImpl;

/**
 * Created by uharris on 2/14/17.
 */

public class CommentInteractor implements CommentProvider.Interactor {

    CommentProvider.DataOutput output;
    CommentRepositoryImpl repository = new CommentRepositoryImpl();

    public CommentInteractor(CommentProvider.DataOutput output) {
        this.output = output;
    }

    @Override
    public void getComments(String id) {
        repository.getComments(id, new RepositoryListener<Response<Comments>>() {
            @Override
            public void onSuccess(Response<Comments> response) {
                output.onGetCommentsSuccess(response);
            }

            @Override
            public void onFailed(String error) {
                output.onGetCommentsFailure(error);
            }
        });
    }
}
