package com.uharris.flickrapp.data.repositories.places;

import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by uharris on 2/14/17.
 */

public interface PlacesService {

    @GET("?method=flickr.places.getInfo")
    Call<Response<Place>> getPlaceDetail(@Query("place_id") String placeId);
}
