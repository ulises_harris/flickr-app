package com.uharris.flickrapp.data.sections.main;

import android.app.Activity;

import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.interactors.main.MainInteractor;
import com.uharris.flickrapp.data.interactors.main.MainProvider;
import com.uharris.flickrapp.data.routing.IBaseRouting;

/**
 * Created by uharris on 2/13/17.
 */

public class MainPresenter implements MainContract.ActionListener, MainProvider.DataOutput {

    MainContract.View view;
    IBaseRouting router;
    MainProvider.Interactor interactor;

    public MainPresenter(MainContract.View view, IBaseRouting router) {
        this.view = view;
        this.router = router;

        interactor = new MainInteractor(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void getPublicPhotos(int page) {
        interactor.getPublicPhotos(page);
    }

    @Override
    public void searchPhotos(int page, String query) {
        interactor.searchPublicPhotos(page, query);
    }

    @Override
    public void onPhotoDetail(Activity activity, Photo photo) {
        router.photoDetail(activity, photo);
    }

    @Override
    public void onGetPhotosSuccess(Response<Photos> response) {
        if(view != null){
            view.onSuccess(response);
        }
    }

    @Override
    public void onGetPhotoFailure(String error) {
        if(view != null){
            view.onFailure(error);
        }
    }
}
