package com.uharris.flickrapp.data.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by uharris on 2/13/17.
 */

public class Response<T> {

    @SerializedName(value="item", alternate={"photos", "place", "comments"})
    T item;
    String stat;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
