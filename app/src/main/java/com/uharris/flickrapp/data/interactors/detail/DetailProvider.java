package com.uharris.flickrapp.data.interactors.detail;

import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;

/**
 * Created by uharris on 2/14/17.
 */

public interface DetailProvider {

    interface Interactor{
        void getPlaceDetail(String id);
    }

    interface DataOutput{
        void onGetPlaceDetailSuccess(Response<Place> response);

        void onGetPlaceDetailFailure(String error);
    }
}
