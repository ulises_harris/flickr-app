package com.uharris.flickrapp.data.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by uharris on 2/14/17.
 */

public class Comment {

    String id;
    @SerializedName("authorname")
    String authorName;
    @SerializedName("iconserver")
    String iconServer;
    @SerializedName("iconfarm")
    int iconFarm;
    @SerializedName("_content")
    String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getIconServer() {
        return iconServer;
    }

    public void setIconServer(String iconServer) {
        this.iconServer = iconServer;
    }

    public int getIconFarm() {
        return iconFarm;
    }

    public void setIconFarm(int iconFarm) {
        this.iconFarm = iconFarm;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
