package com.uharris.flickrapp.data.repositories.places;

import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.repositories.RepositoryListener;
import com.uharris.flickrapp.data.repositories.ServiceManager;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by uharris on 2/14/17.
 */

public class PlaceRepositoryImpl {

    ServiceManager serviceManager = ServiceManager.getInstance();

    public PlaceRepositoryImpl() {
    }

    public void getPlaceDetail(String placeId, final RepositoryListener<Response<Place>> callback){
        Call<Response<Place>> call = serviceManager.getPlacesService().getPlaceDetail(placeId);
        call.enqueue(new Callback<Response<Place>>() {
            @Override
            public void onResponse(Call<Response<Place>> call, retrofit2
                    .Response<Response<Place>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Response<Place>> call, Throwable t) {
                callback.onFailed(t.getLocalizedMessage());
            }
        });
    }
}
