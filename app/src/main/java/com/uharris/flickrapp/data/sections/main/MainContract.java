package com.uharris.flickrapp.data.sections.main;

import android.app.Activity;

import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;

/**
 * Created by uharris on 2/13/17.
 */

public interface MainContract {

    interface View{
        void onSuccess(Response<Photos> response);

        void onFailure(String error);

        void showDialog(boolean show);
    }

    interface ActionListener{
        void onDestroy();

        void getPublicPhotos(int page);

        void searchPhotos(int page, String query);

        void onPhotoDetail(Activity activity, Photo photo);
    }
}
