package com.uharris.flickrapp.data.sections.fullscreen;

import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Response;

/**
 * Created by uharris on 2/14/17.
 */

public interface FullScreenContract {

    interface View{
        void onGetCommentsSuccess(Response<Comments> response);

        void onGetCommentsFailure(String error);
    }

    interface ActionListener{
        void onDestroy();

        void getComments(String id);
    }
}
