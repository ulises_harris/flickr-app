package com.uharris.flickrapp.data.repositories.comments;

import com.uharris.flickrapp.data.entities.Comments;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.repositories.RepositoryListener;
import com.uharris.flickrapp.data.repositories.ServiceManager;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by uharris on 2/14/17.
 */

public class CommentRepositoryImpl {

    ServiceManager serviceManager = ServiceManager.getInstance();

    public CommentRepositoryImpl() {
    }

    public void getComments(String id, final RepositoryListener<Response<Comments>> callback){
        Call<Response<Comments>> call = serviceManager.getCommentService().getComments(id);
        call.enqueue(new Callback<Response<Comments>>() {
            @Override
            public void onResponse(Call<Response<Comments>> call, retrofit2
                    .Response<Response<Comments>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Response<Comments>> call, Throwable t) {
                callback.onFailed(t.getLocalizedMessage());
            }
        });
    }
}
