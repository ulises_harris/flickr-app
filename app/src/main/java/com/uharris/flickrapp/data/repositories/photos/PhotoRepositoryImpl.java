package com.uharris.flickrapp.data.repositories.photos;

import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.repositories.RepositoryListener;
import com.uharris.flickrapp.data.repositories.ServiceManager;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by uharris on 2/13/17.
 */

public class PhotoRepositoryImpl {


    ServiceManager serviceManager = ServiceManager.getInstance();

    public PhotoRepositoryImpl() {
    }

    public void getRecentPhoto(int page, final RepositoryListener<Response<Photos>> callback) {
        Call<Response<Photos>> call = serviceManager.getPhotoService().getPublicPhotos(page);
        call.enqueue(new Callback<Response<Photos>>() {
            @Override
            public void onResponse(Call<Response<Photos>> call, retrofit2
                    .Response<Response<Photos>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Response<Photos>> call, Throwable t) {
                callback.onFailed(t.getLocalizedMessage());
            }
        });
    }

    public void searchPhotos(int page, String query, final RepositoryListener<Response<Photos>>
            callback) {
        Call<Response<Photos>> call = serviceManager.getPhotoService().searchPhotos(page, query);
        call.enqueue(new Callback<Response<Photos>>() {
            @Override
            public void onResponse(Call<Response<Photos>> call, retrofit2
                    .Response<Response<Photos>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<Response<Photos>> call, Throwable t) {
                callback.onFailed(t.getLocalizedMessage());
            }
        });
    }

}
