package com.uharris.flickrapp.data.interactors.main;

import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;

/**
 * Created by uharris on 2/13/17.
 */

public interface MainProvider {

    interface Interactor{
        void getPublicPhotos(int page);

        void searchPublicPhotos(int page, String search);
    }

    interface DataOutput{
        void onGetPhotosSuccess(Response<Photos> response);

        void onGetPhotoFailure(String error);
    }
}
