package com.uharris.flickrapp.data.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by uharris on 2/13/17.
 */

public class Photos {

    int page;
    int pages;
    int perPage;
    int total;
    @SerializedName("photo")
    ArrayList<Photo> photoList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerpage() {
        return perPage;
    }

    public void setPerpage(int perpage) {
        this.perPage = perpage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<Photo> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(ArrayList<Photo> photoList) {
        this.photoList = photoList;
    }
}
