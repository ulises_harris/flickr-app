package com.uharris.flickrapp.data.entities;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by uharris on 2/14/17.
 */

public class Comments {
    @SerializedName("photo_id")
    String photoId;
    @SerializedName("comment")
    ArrayList<Comment> comments;

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }
}
