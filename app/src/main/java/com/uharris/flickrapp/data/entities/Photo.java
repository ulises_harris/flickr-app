package com.uharris.flickrapp.data.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by uharris on 2/13/17.
 */

public class Photo implements Parcelable {
    String id;
    String owner;
    String secret;
    String server;
    int farm;
    @SerializedName("ownername")
    String ownerName;
    String title;
    @SerializedName("dateupload")
    String dateUpload;
    @SerializedName("iconserver")
    String iconServer;
    @SerializedName("iconfarm")
    int iconFarm;
    String latitude;
    String longitude;
    String accuracy;
    @SerializedName("place_id")
    String placeId;
    String woeid;
    Description description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateUpload() {
        return dateUpload;
    }

    public void setDateUpload(String dateUpload) {
        this.dateUpload = dateUpload;
    }

    public String getIconServer() {
        return iconServer;
    }

    public void setIconServer(String iconServer) {
        this.iconServer = iconServer;
    }

    public int getIconFarm() {
        return iconFarm;
    }

    public void setIconFarm(int iconFarm) {
        this.iconFarm = iconFarm;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getWoeid() {
        return woeid;
    }

    public void setWoeid(String woeid) {
        this.woeid = woeid;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public Photo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.owner);
        dest.writeString(this.secret);
        dest.writeString(this.server);
        dest.writeInt(this.farm);
        dest.writeString(this.ownerName);
        dest.writeString(this.title);
        dest.writeString(this.dateUpload);
        dest.writeString(this.iconServer);
        dest.writeInt(this.iconFarm);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.accuracy);
        dest.writeString(this.placeId);
        dest.writeString(this.woeid);
        dest.writeParcelable(this.description, flags);
    }

    protected Photo(Parcel in) {
        this.id = in.readString();
        this.owner = in.readString();
        this.secret = in.readString();
        this.server = in.readString();
        this.farm = in.readInt();
        this.ownerName = in.readString();
        this.title = in.readString();
        this.dateUpload = in.readString();
        this.iconServer = in.readString();
        this.iconFarm = in.readInt();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.accuracy = in.readString();
        this.placeId = in.readString();
        this.woeid = in.readString();
        this.description = in.readParcelable(Description.class.getClassLoader());
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
