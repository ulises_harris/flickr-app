package com.uharris.flickrapp.data.sections.detail;

import android.app.Activity;

import com.uharris.flickrapp.data.entities.Photo;
import com.uharris.flickrapp.data.entities.Place;
import com.uharris.flickrapp.data.entities.Response;

/**
 * Created by uharris on 2/14/17.
 */

public interface DetailContract {
    interface View{
        void onGetPlaceDetailSuccess(Response<Place> response);

        void onGetPlaceDetailFailure(String error);
    }

    interface ActionListener{
        void onDestroy();

        void getPlaceDetail(String id);

        void fullScreenActivity(Activity activity, Photo photo);
    }
}
