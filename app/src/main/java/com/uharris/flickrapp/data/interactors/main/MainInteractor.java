package com.uharris.flickrapp.data.interactors.main;

import com.uharris.flickrapp.data.entities.Photos;
import com.uharris.flickrapp.data.entities.Response;
import com.uharris.flickrapp.data.repositories.RepositoryListener;
import com.uharris.flickrapp.data.repositories.photos.PhotoRepositoryImpl;

/**
 * Created by uharris on 2/13/17.
 */

public class MainInteractor implements MainProvider.Interactor {

    MainProvider.DataOutput output;
    PhotoRepositoryImpl repository = new PhotoRepositoryImpl();

    public MainInteractor(MainProvider.DataOutput output) {
        this.output = output;
    }

    @Override
    public void getPublicPhotos(int page) {
        repository.getRecentPhoto(page, new RepositoryListener<Response<Photos>>() {
            @Override
            public void onSuccess(Response<Photos> response) {
                output.onGetPhotosSuccess(response);
            }

            @Override
            public void onFailed(String error) {
                output.onGetPhotoFailure(error);
            }
        });
    }

    @Override
    public void searchPublicPhotos(int page, String search) {
        repository.searchPhotos(page, search, new RepositoryListener<Response<Photos>>() {
            @Override
            public void onSuccess(Response<Photos> response) {
                output.onGetPhotosSuccess(response);
            }

            @Override
            public void onFailed(String error) {
                output.onGetPhotoFailure(error);
            }
        });
    }
}
