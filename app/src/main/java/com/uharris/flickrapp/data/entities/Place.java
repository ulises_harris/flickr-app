package com.uharris.flickrapp.data.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by uharris on 2/14/17.
 */
public class Place {

    @SerializedName("place_id")
    String id;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
